#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <tracker.h>

////////////////////////////////////////////////////
namespace Ui {
class Dialog;
}
////////////////////////////////////////////////////
class Dialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private:
    Ui::Dialog *ui;

    QTimer* tmrTimer;
    tracker* trkProc;

    tracker::LinePoints Points;

    cv::Mat DrawBlueCircles(cv::Mat matOrignal, std::vector<cv::Vec3f> vecCirclesBlue);
    cv::Mat DrawRedCircles(cv::Mat matOrignal, std::vector<cv::Vec3f> vecCirclesRed);

public slots:

    void processFrame();

private slots:
    void on_btnPause_clicked();


};

#endif // DIALOG_H
