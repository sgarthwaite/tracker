#include "tracker.h"

tracker::tracker()
{  
    capWebcam.open(1);
}

bool tracker::CameraFound()
{
    return capWebcam.isOpened();
}

cv::Mat tracker::ReadFrame()
{
    cv::Mat matOriginal;

    capWebcam.read(matOriginal);

    return matOriginal;
}

cv::Mat tracker::ReduceNoise(cv::Mat matOriginal)
{

    // Uses a Gaussian Blue in order to reduce the noise on the image, thus making an inRange function easier
    cv::GaussianBlur(matOriginal, matOriginal, cv::Size(9, 9), 1.5);

    return matOriginal;

}

std::vector<cv::Vec3f> tracker::DetectBlue(cv::Mat matOriginal)
{
    std::vector<cv::Vec3f> vecCirclesBlue;

    cv::inRange(matOriginal, cv::Scalar(100,0,0), cv::Scalar(256,80,80), matOriginal);
    cv::HoughCircles(matOriginal, vecCirclesBlue, CV_HOUGH_GRADIENT, 2, matOriginal.rows / 4, 100, 50, 10, 400);

    return vecCirclesBlue;
}

std::vector<cv::Vec3f> tracker::DetectRed(cv::Mat matOriginal)
{
    std::vector<cv::Vec3f> vecCirclesRed;

    cv::inRange(matOriginal, cv::Scalar(0,0,120), cv::Scalar(90,90,256), matOriginal);
    cv::HoughCircles(matOriginal, vecCirclesRed, CV_HOUGH_GRADIENT, 2, matOriginal.rows / 4, 100, 50, 10, 400);

    return vecCirclesRed;
}

tracker::LinePoints tracker::FindLine(std::vector<cv::Vec3f> VecCirclesBlue, std::vector<cv::Vec3f> VecCirclesRed)
{

    LinePoints Points;

    // Check that atleast one of each colour is detected.
    if (VecCirclesRed.size() > 0 && VecCirclesBlue.size() > 0) {

        Points.LinePresent = true;

        std::vector<cv::Vec3f>::iterator itrCirclesBlue;
        std::vector<cv::Vec3f>::iterator itrCirclesRed;

        // max possible distance
        float minDist = ( 480 * 480 + 640 * 640 );

        for (itrCirclesBlue = VecCirclesBlue.begin(); itrCirclesBlue != VecCirclesBlue.end(); itrCirclesBlue++) {

            for (itrCirclesRed = VecCirclesRed.begin(); itrCirclesRed != VecCirclesRed.end(); itrCirclesRed++) {

                cv::Point centerRed((int)(*itrCirclesRed)[0], (int)(*itrCirclesRed)[1]);
                cv::Point centerBlue((int)(*itrCirclesBlue)[0], (int)(*itrCirclesBlue)[1]);

                if ((((centerRed.x-centerBlue.x) * (centerRed.x-centerBlue.x)) + ((centerRed.y-centerBlue.y) * (centerRed.y-centerBlue.y))) < minDist) {

                    Points.BluePoint = centerBlue;
                    Points.RedPoint = centerRed;

                    minDist = (((centerRed.x-centerBlue.x) * (centerRed.x-centerBlue.x)) + ((centerRed.y-centerBlue.y) * (centerRed.y-centerBlue.y)));

                }
            }
        }

    } else {

        Points.LinePresent = false;

    }

    return Points;

}
