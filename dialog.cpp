#include "dialog.h"
#include "ui_dialog.h"

#include <QtCore>

////////////////////////////////////////////////////////////////////

Dialog::Dialog(QWidget *parent) : QDialog(parent), ui(new Ui::Dialog){
    ui->setupUi(this);

    trkProc = new tracker;

    // Set up timer and signal/slot
    tmrTimer = new QTimer(this);
    connect(tmrTimer, SIGNAL(timeout()), this, SLOT(processFrame()));

    // If camera isn't found, display error and quit
    if(!trkProc->CameraFound()){
        ui->txtXYReadings->appendPlainText("Error: Webcam not accessed successfully");
        return;
    }

    // Start the timer with 100ms delay
    tmrTimer->start(100);

}

////////////////////////////////////////////////////////////////////

Dialog::~Dialog(){
    delete ui;
}

////////////////////////////////////////////////////////////////////

void Dialog::processFrame() {

    //Declarations
    cv::Mat matOriginal;
    cv::Mat matProcessed;

    std::vector<cv::Vec3f> vecCirclesRed;
    std::vector<cv::Vec3f> vecCirclesBlue;

    // Clear previous readings
    ui->txtXYReadings->clear();

    // Get next frame and make sure it isn't empty
    matOriginal = trkProc->ReadFrame();
    if (matOriginal.empty()) return;

    // Use gaussian blur to minimize noise
    matProcessed = trkProc->ReduceNoise(matOriginal);

    // Detect blue and red circles and output coordinates and radii to vectors
    vecCirclesBlue = trkProc->DetectBlue(matProcessed);
    vecCirclesRed = trkProc->DetectRed(matProcessed);

    // Draw circles to original image
    if (vecCirclesBlue.size()>0) matOriginal = DrawBlueCircles(matOriginal, vecCirclesBlue);
    if (vecCirclesRed.size()>0) matOriginal = DrawRedCircles(matOriginal, vecCirclesRed);

    // Look for a line between red & blue centers
    Points = trkProc->FindLine(vecCirclesBlue,vecCirclesRed);

    // If there is a line to be shown (both red and blue circles exist
    if (Points.LinePresent) {
        cv::line(matOriginal,Points.RedPoint,Points.BluePoint,cv::Scalar(0,0,0),3,8,0);
    }

    // Convert image from BGR to RGB and display in ui
    cv::cvtColor(matOriginal, matOriginal, CV_BGR2RGB);
    QImage qimgOriginal((uchar*)matOriginal.data, matOriginal.cols, matOriginal.rows, matOriginal.step, QImage::Format_RGB888);
    ui->lblOriginal->setPixmap(QPixmap::fromImage(qimgOriginal));

}

cv::Mat Dialog::DrawBlueCircles(cv::Mat matOriginal, std::vector<cv::Vec3f> vecCirclesBlue) {
    std::vector<cv::Vec3f>::iterator itrCirclesBlue;
    for (itrCirclesBlue = vecCirclesBlue.begin(); itrCirclesBlue != vecCirclesBlue.end(); itrCirclesBlue++) {
        ui->txtXYReadings->appendPlainText(QString("Blue ball position x = ") + QString::number((*itrCirclesBlue)[0]).rightJustified(4, ' ') +
                                           QString(", y = ") + QString::number((*itrCirclesBlue)[1]).rightJustified(4, ' ') +
                                           QString(", radius = ") + QString::number((*itrCirclesBlue)[2], 'f',3).rightJustified(7, ' '));

        cv::circle(matOriginal, cv::Point((int)(*itrCirclesBlue)[0], (int)(*itrCirclesBlue)[1]), 3, cv::Scalar(0,255,0), CV_FILLED);
        cv::circle(matOriginal, cv::Point((int)(*itrCirclesBlue)[0], (int)(*itrCirclesBlue)[1]), (int)(*itrCirclesBlue)[2], cv::Scalar(255,0,0),3);

    }
    return matOriginal;
}


cv::Mat Dialog::DrawRedCircles(cv::Mat matOriginal, std::vector<cv::Vec3f> vecCirclesRed) {
    std::vector<cv::Vec3f>::iterator itrCirclesRed;
    for (itrCirclesRed = vecCirclesRed.begin(); itrCirclesRed != vecCirclesRed.end(); itrCirclesRed++) {
        ui->txtXYReadings->appendPlainText(QString("Red ball position x = ") + QString::number((*itrCirclesRed)[0]).rightJustified(4, ' ') +
                                           QString(", y = ") + QString::number((*itrCirclesRed)[1]).rightJustified(4, ' ') +
                                           QString(", radius = ") + QString::number((*itrCirclesRed)[2], 'f',3).rightJustified(7, ' '));

        cv::circle(matOriginal, cv::Point((int)(*itrCirclesRed)[0], (int)(*itrCirclesRed)[1]), 3, cv::Scalar(0,255,0), CV_FILLED);
        cv::circle(matOriginal, cv::Point((int)(*itrCirclesRed)[0], (int)(*itrCirclesRed)[1]), (int)(*itrCirclesRed)[2], cv::Scalar(0,0,255),3);


    }
    return matOriginal;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Dialog::on_btnPause_clicked(){

    if(tmrTimer->isActive() == true){
        tmrTimer->stop();
        ui->btnPause->setText("Resume");
    } else {
        tmrTimer->start(20);
        ui->btnPause->setText("Pause");
    }

}
