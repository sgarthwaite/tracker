#include <QtGui/QApplication>
#include "dialog.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    bool GUI = true;

    for(int i = 0; i < argc; i++)
    {
        if (argv[i] == "--no-gui")
        {
            GUI = false;
            break;
        }
    }

    if (GUI == true){
        static Dialog w;
        w.show();
    } else {
        //ui_commandline c;
    }

    return a.exec();
}
