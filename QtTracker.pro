#-------------------------------------------------
#
# Project created by QtCreator 2012-10-27T01:51:35
#
#-------------------------------------------------

QT       += core gui

TARGET = QtTracker
TEMPLATE = app


SOURCES += main.cpp\
    tracker.cpp \
    ui_commandline.cpp \
    dialog.cpp

HEADERS  += \
    tracker.h \
    ui_commandline.h \
    dialog.h

FORMS    += dialog.ui

unix {
    INCLUDEPATH += /usr/include/opencv
    LIBS += -L/usr/lib \
        -lopencv_core \
        -lopencv_imgproc \
        -lopencv_highgui \
        -lopencv_ml \
        -lopencv_video \
        -lopencv_features2d \
        -lopencv_calib3d \
        -lopencv_objdetect \
        -lopencv_contrib \
        -lopencv_legacy \
        -lopencv_flann
}


windows {
    INCLUDEPATH += C:\\OpenCV2.4\\opencv\\build\\include

    LIBS += -LC:\\OpenCV2.4\\build\\lib\\Debug \
    -lopencv_calib3d240d \
    -lopencv_contrib240d \
    -lopencv_core240d \
    -lopencv_features2d240d \
    -lopencv_flann240d \
    -lopencv_gpu240d \
    -lopencv_highgui240d \
    -lopencv_imgproc240d \
    -lopencv_legacy240d \
    -lopencv_ml240d \
    -lopencv_nonfree240d \
    -lopencv_objdetect240d \
    -lopencv_photo240d \
    -lopencv_stitching240d \
    -lopencv_ts240d \
    -lopencv_video240d \
    -lopencv_videostab240d
}
