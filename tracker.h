#ifndef TRACKER_H
#define TRACKER_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

class tracker
{

public:

    struct LinePoints {
        bool LinePresent;
        cv::Point RedPoint;
        cv::Point BluePoint;
    };


private:

    cv::VideoCapture capWebcam;

public:

    bool CameraFound();

    cv::Mat ReadFrame();
    cv::Mat ReduceNoise(cv::Mat matOriginal);

    std::vector<cv::Vec3f> DetectBlue(cv::Mat matOriginal);
    std::vector<cv::Vec3f> DetectRed(cv::Mat matOriginal);

    LinePoints FindLine(std::vector<cv::Vec3f> VecCirclesBlue ,std::vector<cv::Vec3f> VecCirclesRed);


public:
    tracker();
};

#endif // TRACKER_H
