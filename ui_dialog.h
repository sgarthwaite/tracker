/********************************************************************************
** Form generated from reading UI file 'dialog.ui'
**
** Created: Sun 11. Nov 01:17:43 2012
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_H
#define UI_DIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPlainTextEdit>
#include <QtGui/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_Dialog
{
public:
    QLabel *lblOriginal;
    QPushButton *btnPause;
    QPlainTextEdit *txtXYReadings;

    void setupUi(QDialog *Dialog)
    {
        if (Dialog->objectName().isEmpty())
            Dialog->setObjectName(QString::fromUtf8("Dialog"));
        Dialog->resize(660, 658);
        lblOriginal = new QLabel(Dialog);
        lblOriginal->setObjectName(QString::fromUtf8("lblOriginal"));
        lblOriginal->setGeometry(QRect(10, 10, 640, 480));
        lblOriginal->setAutoFillBackground(false);
        lblOriginal->setStyleSheet(QString::fromUtf8("border:solid; border-color:rgb(20, 117, 181);border-width:1px;"));
        btnPause = new QPushButton(Dialog);
        btnPause->setObjectName(QString::fromUtf8("btnPause"));
        btnPause->setGeometry(QRect(10, 500, 90, 150));
        btnPause->setAutoFillBackground(false);
        btnPause->setStyleSheet(QString::fromUtf8("border:solid;\n"
"border-color:black;\n"
"border-width:1px;\n"
"background-color:#FFFFFF;\n"
"\n"
"\n"
""));
        btnPause->setFlat(true);
        txtXYReadings = new QPlainTextEdit(Dialog);
        txtXYReadings->setObjectName(QString::fromUtf8("txtXYReadings"));
        txtXYReadings->setGeometry(QRect(110, 500, 540, 150));
        txtXYReadings->setAutoFillBackground(true);
        txtXYReadings->setStyleSheet(QString::fromUtf8(""));
        txtXYReadings->setFrameShape(QFrame::Panel);
        txtXYReadings->setFrameShadow(QFrame::Plain);
        txtXYReadings->setReadOnly(true);
        txtXYReadings->setBackgroundVisible(false);

        retranslateUi(Dialog);

        QMetaObject::connectSlotsByName(Dialog);
    } // setupUi

    void retranslateUi(QDialog *Dialog)
    {
        Dialog->setWindowTitle(QApplication::translate("Dialog", "Dialog", 0, QApplication::UnicodeUTF8));
        lblOriginal->setText(QApplication::translate("Dialog", "TextLabel", 0, QApplication::UnicodeUTF8));
        btnPause->setText(QApplication::translate("Dialog", "Pause", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Dialog: public Ui_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_H
